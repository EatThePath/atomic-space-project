# Atomic Space Project
Atomic Space Project's purpose is to create games involving space travel and combat that do not handwave the scales of time, distance or speed that should entail, without losing playability. I have designs two such games in mind, but I'll detail those elsewhere. It's quite possible that this could provide a foundation for others to make like-minded games, as well.

What the Atomic Space is now, is a simple numerical integration engine for orbital motion. Current development effort is aimed at enabling the testing of different integration methods, and the baking of integrated motion to a lightweight format suitable for use in gameplay.

I am learning Rust as I go, and the while I've worked on this before in other forms this incarnation of the project is pretty early days. If you see something in the code that could be improved, feel free to let me know or even offer a fix.

If you wish to discuss the project directly, you can best find me at https://discord.gg/hT2HwAF.

# Conduct

I may eventually replace it with something more bespoke, but until then, see https://www.rust-lang.org/policies/code-of-conduct

# Why not Github?

I'm not entirely comfortable with github's dominance of the repository hosting space, so I want to give other options a chance. I have also had bad experinces with Github's fancier features not being entirely stable, and I want to avoid becoming dependant on those.

That said, I use Github and I recognize that it's the default. The decision of where to host the repository is neither absolute or final, and if being here ends up posing any barriers to contributors I'll explore solutions, either moving or setting up a mirror.

# Project history

I have worked on this effort in the past as a C# and unity project. Simple game prototyping proved promising, but my time was pulled away from it. I made the decision to rewrite it in rust as a chance to overcome some foundational limitations in the last effort, and to avoid some intractable problems with working within Unity.