//The dimensioned crate provides a way to have physics math type-checked to ensure all the equations are physically valid and don't mix units in nonsensical ways. 
//This is somewhat provisional and may be dropped eventually, but has caught a few issues for me so I'm going to keep trying with it.
//The vector crate I'm using is 
extern crate dimensioned as dim;
//This is largely so that I can swap between f64 and f32 to test out if that matters
pub type PhysQuant = f64;
//This gets us access to the pre-defined SI units.
use dim::si::SI;
use dim::tarr;
use dim::typenum::{N1, N2, P3, Z0};
use calcify::consts;
pub use dimensioned::*;

//use std::ops::Deref;
pub type Meter = dim::si::Meter<PhysQuant>;
pub type MeterSec = dim::si::MeterPerSecond<PhysQuant>;
pub type MeterSec2 = dim::si::MeterPerSecond2<PhysQuant>;
pub type KGram = dim::si::Kilogram<PhysQuant>;
pub type Second = dim::si::Second<PhysQuant>;
pub type Unitless = dim::si::Unitless<PhysQuant>;

//the universal constant of gravity, G, has messy units that aren't directly represented by any of dim's existing types, so this creates that.
pub type BigGUnit = SI<PhysQuant, tarr![P3, N1, N2, Z0, Z0, Z0, Z0]>;
//mu, or 'standard gravitational parameter', is the mass of two objects times G. Like G, we need to build our own unit for it.
pub type GM = SI<PhysQuant, tarr![P3, Z0, N2, Z0, Z0, Z0, Z0]>;

//We're importing these consts from calcify, but in our types.
pub const BIG_G: BigGUnit = BigGUnit::new(consts::BIG_G as PhysQuant);
pub const PI: PhysQuant = consts::PI;
pub const TAU: PhysQuant=  2.0 * consts::PI ;

pub const M_E:KGram = KGram::new(5.97219e24);
pub const M_J:KGram = KGram::new(1.89818722e27); 
pub const M_S:KGram = KGram::new(1.9885e30);