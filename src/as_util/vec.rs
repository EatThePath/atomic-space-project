use super::units::*;
use vector3d::Vector3d;

pub type UnitlessVec = Vector3d<Unitless>;
pub type PosVec = Vector3d<Meter>;
pub type VelVec = Vector3d<MeterSec>;
pub type AclVec = Vector3d<MeterSec2>;

//The vector3d crate does the basic grunt work of making a generic vector type, but has some gaps we need to fill in.
pub trait GoodVec<I> {
    type Udim;
    type Squared;
    fn mag(&self) -> I;
    fn mag_squared(&self) -> Self::Squared;
    fn norm(&self) -> Vector3d<Self::Udim>;
}

use dimensioned::{Dimensioned, Dimensionless};
use dimensioned::traits::Sqrt;
use std::ops::{Add, Div, Mul};

impl<I, S, U> GoodVec<I> for Vector3d<I>
where
    I: Copy + Dimensioned + Mul<I, Output = S> + Div<I, Output = U>,
    S: Add<S, Output = S> + Sqrt<Output = I>,
    U: Dimensionless,
{
    type Udim = U;
    type Squared = S;
    fn mag(&self) -> I {
        (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
    }
    fn mag_squared(&self) -> S {
        self.x * self.x + self.y * self.y + self.z * self.z
    }
    fn norm(&self) -> Vector3d<U> {
        *self / self.mag()
    }
}
