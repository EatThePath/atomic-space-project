use crate::prelude::*;

#[cfg(test)]
mod tests{
    #[test]
    //Uses state vectors and elements from JPL Horizons, earth and sun centers at 19000101, to round trip from state vectors through a keplerian orbit and in doing so verify the correctness of both conversions.

    fn kepler_check(){
        use crate::as_util::kepler::*;
        let p = PosVec::new( 
            Meter::new(-28977759517.45683-476196387.1120387),
            Meter::new(145067848582.1868-952387633.8624872),
            Meter::new(16564650.12631565--15440659.51675491));
        let v = VelVec::new(
                MeterSec::new(-29698.65396388122--12.729339273302159),
                MeterSec::new(-6068.632814738924-6.5503807953202315),
                MeterSec::new(-2.840161309361822-0.30276441245857516));
        let k = KeplerElements::from_vectors(M_S, p, v);
        println!("earth orbit test\nInput vectors\np={:.0}\nv={:.0}",p,v);
        println!("kepler orbit\na = {:.1} au\norbital period {:.2} years\n{}"
        ,k.a / AU,k.period(M_S).unwrap()/(60.0*60.0*24.0*365.24*S)
        ,k.print());
        println!("reference orbit    e = {:.6} i = {:.6} LoAN = {:.6} AoP = {:.6} T_A = {:.6}",
            1.766239629114153E-02,
            1.380925355555036E-02,
            3.460757101127622E+02,
            1.163343909536250E+02,
            3.591407914860750E+02

        );
        let ta = k.true_anomaly.unwrap();
        let p2 = k.relative_position(ta);
        let v2 = k.relative_velocity(M_S,ta);
        let p_diff = p2 - p;
        let v_diff = v2 - v;
        println!("round trip output vectors");
        println!("p={:.2}",p2 );
        println!("v={:.2}",v2 );

        println!("p error={:.2} {:.2e} diff, {:.2}x",p_diff, p_diff.mag(), p_diff.mag()/p.mag());
        println!("v error={:.2} {:.2e} diff, {:.2}x",v_diff, v_diff.mag(), v_diff.mag()/v.mag());


    }
}
struct KeplerOrbit {
    elements: KeplerElements,
    parent_mass: KGram,
    self_mass: KGram,
    start_time: Second,
}

#[derive(Default)]
pub struct KeplerElements {
    pub a: Meter,
    pub e: Unitless,
    pub i: Unitless,
    pub node: PhysQuant,
    pub arg_of_peri: PhysQuant,
    pub true_anomaly: Option<PhysQuant>,
}

impl KeplerElements {
    ///
    /// Returns the closest and farthest orbital altitudes this orbit can reach.
    ///
    pub fn range(&self) -> std::ops::Range<Meter> {
        (1.0 - self.e) * self.a..(1.0 + self.e) * self.a
    }
    pub fn period(&self, parent: KGram) -> Option<Second> {
        match self.e.value() {
            e if *e >= 1.0 => None,
            e if *e < 1.0 => Some(TAU * ((self.a * self.a * self.a) / (BIG_G * parent)).sqrt()),
            _ => None,
        }
    }
    pub fn print(&self) -> String {
        format!(
            "a = {:.0} e = {:.6} i = {:.6} LoAN = {:.6} AoP = {:.6} T_A = {:.6?}",
            self.a,
            self.e, 
            self.i.value().to_degrees(), 
            self.node.to_degrees(), 
            self.arg_of_peri.to_degrees(), 
            self.true_anomaly.unwrap().to_degrees()
        )
    }
    ///
    /// Vectors are relative to the parent's vectors.
    /// From c# version: "this math pulled from a pdf from one cdeagle that I've lost, it's seved me well so far though."
    /// 
    pub fn from_vectors(parent: KGram, r: PosVec, v: VelVec) -> KeplerElements {
        let mu = BIG_G * parent;
        let angular_momentum = r.cross(v);
        let node_vector = vector3d::Vector3d::<PhysQuant>::new(0.0, 0.0, 1.0).cross(angular_momentum);
        let eccentricity_vector =(r * 
            (v.mag_squared() - mu/(r.mag())) 
            - v * r.dot(v) )/mu;

        let e = eccentricity_vector.mag();
        let i = Unitless::new((angular_momentum.z / angular_momentum.mag()).acos());

        let specific_orbital_energy = v.mag_squared()/2.0 - (mu / r.mag());

        //let mut p = angular_momentum.mag_squared()/mu;
        let mut a = Meter::new(f64::INFINITY);

        if (e-1.0).abs()>Unitless::new(PhysQuant::EPSILON){
            a = (mu * -1.0) / (specific_orbital_energy * 2.0);
            //p = a * ( 1.0- e*e)
        };
        let mut node = (node_vector.x / node_vector.mag()).acos();
        if *node_vector.y.value_unsafe() < 0.0 {
            node = TAU-node;
        }

        let mut arg_of_peri = (
            node_vector.dot(eccentricity_vector)
            / (e*node_vector.mag())).acos() ;
        if *node_vector.z.value_unsafe() < 0.0 {
            arg_of_peri = TAU - arg_of_peri;
    
        }
        let mut true_anomaly = (eccentricity_vector.dot(r) / (e*r.mag())).acos();

        if *r.dot(v).value_unsafe() < 0.0{
            true_anomaly = TAU - true_anomaly;
        }

        KeplerElements {
            a,
            e,
            i,
            node,
            arg_of_peri,
            true_anomaly: Some(true_anomaly),
            //..Default::default()
        }
    }
    //largely refercing https://github.com/RazerM/orbital/tree/0.7.0
    pub fn relative_velocity(&self,parent:KGram,true_anomaly:PhysQuant) -> VelVec
    {
        let a = self.a;
        let e = self.e.value(); 
        let o = self.node;
        let w = self.arg_of_peri;
        let i = self.i;
        let v = true_anomaly;
        let p = a * (1.0 - (e * e));
        let g = BIG_G * parent;
        let t2 = w+v;
        let r =  (g / p).sqrt() * -1.0 ;
        let x = r * (
            o.cos() * ((w + v).sin()
                        +
                        e * w.sin())
            +
            o.sin() * i.cos() * (t2.cos() + e * w.cos()))
            ;
        let y = r * (
            o.sin() * ((w + v).sin() 
                        + e * w.sin()) 
            - o.cos() * i.cos() * (t2.cos() + e * w.cos()));
        let z = r * (-t2.cos()*i.sin());
        VelVec{x,y,z}
    }

    //largely refercing https://github.com/RazerM/orbital/tree/0.7.0
    pub fn relative_position(&self,true_anomaly:PhysQuant) -> PosVec{
        let a = self.a;
        let e = self.e; 
        let v = true_anomaly;
        let i = self.i;
        let o = self.node;
        let w = self.arg_of_peri;
        let p = a * (1.0 - e * e);
        let r = p/ (1.0 +e * v.cos());
        let t2 = w+v;
        let x = r * (
                o.cos() * (t2).cos() 
                -
                o.sin() * i.cos() * (t2).sin());
        let y = r * (
                o.sin() * (t2).cos() 
                +
                o.cos() * i.cos() * (t2).sin());
        let z = r * i.sin() * (t2).sin();
    
        PosVec{x,y,z}
    }
    
    pub fn soi_radius(&self,_mass:KGram,_parent:KGram) -> Meter{
        self.a * (1.0)
    }
}
