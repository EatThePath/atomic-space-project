#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release
#[macro_use]
mod as_util;
pub mod prelude;
use crate::prelude::*;

use eframe::egui;
use std::time::{Instant,Duration};
use std::sync::{Arc, Mutex};
use std::thread;
mod async_interface;
use async_interface::*;
mod sim;
use sim::*;
mod basic_state_types;
use basic_state_types::*;

use std::collections::BTreeMap;
use clap::Parser;

/// Search for a pattern in a file and display the lines that contain it.
#[derive(Parser)]
#[clap()]
struct Args {
    #[clap(short, long, value_parser)]
    system: Option<String>,
}



struct SimGUI {
    asi:Arc<Mutex<Interface>>,
    done: bool,
    display: String,
    report:Option<RichStateReport>,
}

impl Default for SimGUI {
    fn default() -> Self {
        Self {
            asi: Arc::new(Mutex::new(Interface {
                ui_end: false,
                sim_end: false,
                ..Default::default()
            })),
            done:false,
            display:String::new(),
            report:None
        }
    }
}
//generalized form of update
//  Pointer to input(s)
//  pointer to target(s)
//  function to draw
impl eframe::App for SimGUI {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.horizontal(|ui|{
            match self.report.clone() {
                None => (),
                Some(report) => {
                    let mut lines= BTreeMap::<u16,RichStateReportItem>::new();
                    for (k,l) in report.report_lines{
                        lines.insert(k, l);
                    }
                        ui.vertical(|ui|{
                            for l in lines.values(){
                                    ui.label(format!("{},",l.name));
                                }
                        });
                        ui.vertical(|ui|{
                            for l in lines.values(){
                                    ui.label(format!("{:.2e}",l.mass));
                                }
                        });
                        ui.vertical(|ui|{
                            for l in lines.values(){
                                ui.label(format!("{} states", l.count));
                            }
                        });
                        ui.vertical(|ui|{
                            for l in lines.values(){
                                ui.label(format!("{} - {}", l.first_time, l.last_time));
                            }
                        });
                    },
                }
            });
            ui.label(self.display.to_string());

        });
        thread::sleep(Duration::from_millis(7));
        let i = self.asi.lock().unwrap();
        self.done = i.sim_end;
        self.display = i.report.clone();
        self.report  = i.report2.clone();
        ctx.request_repaint();
    }
}



fn main() {
    let interface = Arc::new(Mutex::new(Interface {
        ui_end: false,
        sim_end: false,
        ..Default::default()
    }));
    let shi = Arc::clone(&interface);
    thread::spawn(move || {
        sim(shi);
    });
    let options = eframe::NativeOptions::default();
    let sg = SimGUI{asi:interface,..Default::default()};
    eframe::run_native(
        "My egui App",
        options,
        Box::new(|_cc| Box::new(sg)),
    );
}

fn sim(interface: Arc<Mutex<Interface>>) {
    let args = Args::parse();
    let mut u: Simulation;

    match args.system {
        Some(name) => u = Simulation::load_system(&name),
        None => u = Simulation::load_system(&"A".to_string()),
    }

    let run_start = Instant::now();
    let simsteps = 1000;
    let run_target = 365.0 * DAY * 20.0 * 10.0;
    let mut terminate = false;
    let mut report_string ;
    let mut step_end = 0.0 * S;
    while !terminate && step_end < run_target {
        step_end = u.advance_steps(simsteps);

        let run_end = Instant::now();
        let realtime = (run_end - run_start).as_secs_f64();

        report_string = format!(
            "Overall sim {:.2e} sim seconds, or {}\nRun {:.2} real seconds,\nTime compression {:.2e}x",
            step_end,
            pretty_time(step_end),
            realtime,
            step_end / realtime
        );
        let mut i = interface.lock().unwrap();
        terminate = i.ui_end;
        i.report = report_string.clone();
        i.report2 = Some(u.rich_report());
    }
    let mut i = interface.lock().unwrap();
    i.sim_end = true;
}
