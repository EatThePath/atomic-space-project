#[macro_use]
pub mod units;
pub mod vec;
pub mod kepler;

use units::*;


pub fn pretty_time(input: Second) -> String {
    let mut seconds = input.value_unsafe;
    let years = (seconds / 31557600.0).floor();
    seconds -= years * 31557600.0;
    let days = (seconds / 86400.0).floor();
    seconds -= days * 86400.0;
    let hours = (seconds / (60.0 * 60.0)).floor();
    seconds -= hours * (60.0 * 60.0);
    let minutes = (seconds / 60.0).floor();
    seconds -= minutes * 60.0;
    format!("{}y {}d {}h {}m {:.2}s", years, days, hours, minutes, seconds)
}

