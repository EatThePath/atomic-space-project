
use crate::basic_state_types::*;
use crate::prelude::*;
mod sim_object;
use crate::sim::sim_object::timeline::*;
use crate::sim::sim_object::*;
//use crate::sim::timeline;
use csv::ReaderBuilder;
use std::collections::hash_map::HashMap;
use std::{fs, vec};
mod tests;

const STEP_BASE: PhysQuant = 2.0;
//use crate::sim::timeline::{*};

#[derive(Clone, Default)]
struct RollingGravMotion {
    id: ObjectID,
}
impl RollingGravMotion {
    /**************************************************************************
     *
     */
    fn step(
        self: &RollingGravMotion,
        start: InstantState,
        snapshot: &SystemSnapshot,
        lookup: &BodyLookup,
        settings: &SimSettings,
    ) -> Result<InstantState, bool> {
        //println!("\nSTEP {}\n starting {}",lookup.get(&self.id).unwrap().name ,start);
        let acl = snapshot.forces_at_location(start.pos, Some(self.id), lookup);
        //println!("acl {}",acl);
        //let step = 0.01*S;
        let step = match settings.force_step {
            None => {
                ((0.5 * MPS) / acl.mag())
                    .value_unsafe
                    .clamp(1.0, 60.0 * 60.0 * 24.0)
                    * S
            }
            Some(t) => t,
        };
        //println!("step {}",step);
        //println!("step unsafe {}",step_us);
        //println!("step {}",step);
        let vel = start.vel + acl * step;
        //println!("vel {}",acl);
        let pos = start.pos + vel * step;
        //println!("pos {}",pos);
        let next = InstantState {
            time: start.time + step,
            pos,
            vel,
            acl,
        };
        //println!("next {}\n\n***",next);
        Ok(next)
    }

    /**************************************************************************
     *
     */

    /**************************************************************************
     *
     */
    fn step_size(level: i32) -> PhysQuant {
        STEP_BASE.powf(level as PhysQuant)
    }
}

impl SystemSnapshot {
    /**************************************************************************
     *
     */
    fn forces_at_location(
        self: &SystemSnapshot,
        target: PosVec,
        ignore: Option<ObjectID>,
        lookup: &BodyLookup,
    ) -> AclVec {
        let mut force: AclVec = AclVec {
            x: 0.0 * MPS2,
            y: 0.0 * MPS2,
            z: 0.0 * MPS2,
        };
        let ignored = ignore.is_some();
        let ignored_id = ignore.unwrap();
        //println!("object {} FORCES AT LOCATION",ignored_id);
        for other in self.system.iter() {
            if !ignored || other.body != ignored_id {
                let other_obj = &lookup[&other.body];
                let sep = other.state.pos - target;
                //println!("{:.2e} vs {:.2e}",(sep.mag()*sep.mag()),sep.mag_squared());
                //println!("{:.2e} vs {:.2e}",(BIG_G*other.body.mass),other.body.gm);
                //let d = sep.mag();
                //let a = sep.norm() * ((BIG_G*other.body.mass) / (d*d));
                let d2 = sep.mag_squared();
                let a = sep.norm() * (other_obj.gm / d2);
                //println!("  Using {}",other.body.id);
                force = force + a;
            } // else {
              //println!("  Skipping {}",other.body.id);
              //}
              //println!("    accel = {}",force.mag());
        }
        force
    }

    /**************************************************************************
     *
     */
    fn extrapolate_forward(self: &SystemSnapshot, forward: Second) -> SystemSnapshot {
        let mut future: SystemSnapshot = SystemSnapshot {
            ..Default::default()
        };
        for snap in self.system.iter() {
            future.system.push(Snapshot {
                body: snap.body,
                state: snap
                    .state
                    .extrapolate_to_timestamp(snap.state.time + forward)
                    .unwrap(),
            });
        }
        future
    }
}

/**************************************************************************
 **************************************************************************
 *
 */
/**************************************************************************
 **************************************************************************
 *
 */
pub struct Simulation {
    data: Vec<SimObject>,
    rules: Vec<SimConstraints>,
    bodies: BodyLookup,
    settings: SimSettings,
}
impl Default for Simulation {
    fn default() -> Self {
        Simulation {
            data: vec![],
            rules: vec![],
            bodies: BodyLookup::new(),
            settings: SimSettings {
                cos_g: BIG_G,
                load_gm: true,
                force_step: None,
            },
        }
    }
}
pub struct SimSettings {
    pub cos_g: BigGUnit,
    pub load_gm: bool,
    pub force_step: Option<Second>,
}
impl Simulation {
    /**************************************************************************
     *
     */

    pub fn new_circular_test() -> Simulation {
        let mut sim = Simulation {
            ..Default::default()
        };
        let star_start = InstantState {
            pos: PosVec::new(0.0 * M, 0.0 * M, 0.0 * M),
            vel: VelVec::new(0.0 * MPS, 0.0 * MPS, 0.0 * MPS),
            ..Default::default()
        };

        sim.add_body(star_start, String::from("center"), 1.0 * KG, None);
        let k = kepler::KeplerElements {
            a: AU,
            e: Unitless::new(0.0),
            i: Unitless::new(0.0),
            node: 0.0,
            arg_of_peri: 0.0,
            true_anomaly: Some(0.0),
        };
        let earth_start = InstantState {
            pos: k.relative_position(0.0),
            vel: k.relative_velocity(M_S, 0.0),
            ..Default::default()
        };
        sim.add_body(earth_start, String::from("follow"), 0.0 * KG, None);

        sim.add_constraint(SimConstraints::Distance {
            min: 0.9999 * AU,
            max: 1.0001 * AU,
            a: 0,
            b: 1,
            at: k.period(M_S).unwrap(),
        });

        //let sv = vel:VelVec::new((MU / 1.0_f64).powf(0.5) * MPS, 0.0*MPS,0.0*MPS);
        sim
    }
    pub fn load_system(sysname: &String) -> Simulation {
        let mut sys = Simulation::default();
        sys.load_bodies(sysname);
        sys.load_vector_start(sysname);
        //   sys.load_rules(sysname);
        sys
    }
    fn load_bodies(&mut self, sysname: &String) {
        let data = fs::read_to_string(format!("./systems/{}.bod", sysname)).unwrap();
        let mut reader = ReaderBuilder::new()
            .has_headers(false)
            .from_reader(data.as_bytes());
        let lines = reader.records();
        for (idx, result) in (0_u16..).zip(lines) {
            let result = result.unwrap();
            let index = result.get(0).unwrap().parse::<u16>().unwrap();
            let name = String::from(result.get(1).unwrap());
            let mass = 1.0 * KG * result.get(2).unwrap().parse::<PhysQuant>().unwrap();
            if index != idx {
                panic!("bad body index for {}", name);
            }
            let gm = match result.get(3) {
                Some(v) => GM::new(v.parse::<PhysQuant>().unwrap()),
                None => BIG_G * mass,
            };
            let this = BodyDef {
                index,
                name,
                mass,
                gm,
            };
            self.bodies.insert(idx, this);
        }
    }

    fn load_vector_start(&mut self, sysname: &String) {
        let data = fs::read_to_string(format!("./systems/{}.stvec", sysname)).unwrap();
        let mut reader = ReaderBuilder::new()
            .has_headers(false)
            .from_reader(data.as_bytes());
        let lines = reader.records();
        for (_, result) in (0_u16..).zip(lines) {
            let result = result.unwrap();
            let index = result.get(0).unwrap().parse::<u16>().unwrap();
            let this_start = InstantState {
                pos: PosVec {
                    x: Meter::new(result.get(1).unwrap().parse::<PhysQuant>().unwrap()),
                    y: Meter::new(result.get(2).unwrap().parse::<PhysQuant>().unwrap()),
                    z: Meter::new(result.get(3).unwrap().parse::<PhysQuant>().unwrap()),
                },
                vel: VelVec {
                    x: MeterSec::new(result.get(4).unwrap().parse::<PhysQuant>().unwrap()),
                    y: MeterSec::new(result.get(5).unwrap().parse::<PhysQuant>().unwrap()),
                    z: MeterSec::new(result.get(6).unwrap().parse::<PhysQuant>().unwrap()),
                },
                time: 0.0 * S,
                ..Default::default()
            };
            let this_body = SimObject::new_new(index, this_start);
            self.data.push(this_body);
        }
    } /*
          fn load_rules(&mut self, sysname: &String) {
              let data = fs::read_to_string(format!("./systems/{}.cond", sysname)).unwrap();
              let mut reader = ReaderBuilder::new()
                  .has_headers(false)
                  .from_reader(data.as_bytes());
              let lines = reader.records();
              for (_, result) in (0_u16..).zip(lines) {
                  let result = result.unwrap();
                /*   let this_rule = SimConstraints::Distance {
                      min: Meter::new(result.get(2).unwrap().parse::<PhysQuant>().unwrap()),
                      max: Meter::new(result.get(3).unwrap().parse::<PhysQuant>().unwrap()),
                      a: result.get(0).unwrap().parse::<u16>().unwrap(),
                      b: result.get(1).unwrap().parse::<u16>().unwrap(),
                  };*/
                  self.rules.push(this_rule);
              }
          }
      */
    /**************************************************************************
     *
     */
    fn step(&mut self) {
        let mut step_start = Second::new(PhysQuant::MAX);
        for o in self.data.iter() {
            if !o.is_empty() {
                step_start = Second::new(PhysQuant::min(
                    step_start.value_unsafe,
                    o.last_state().unwrap().time.value_unsafe,
                ));
            }
        }
        let start_state = &(self.snapshot(step_start));
        for o in self.data.iter_mut() {
            o.step(step_start, start_state, &(self.bodies), &self.settings);
        }
        for o in self.data.iter_mut() {
            o.post();
        }
    }

    fn add_body(&mut self, start: InstantState, name: String, mass: KGram, gm: Option<GM>) -> u16 {
        //let k: Vec<u16> =
        let index = match self.bodies.is_empty() {
            true => 0,
            false => {
                let mut v: Vec<u16> = self.bodies.keys().copied().collect();
                v.sort();
                let i = v.last().unwrap() + 1;
                i
            }
        };
        self.bodies.insert(
            index,
            BodyDef {
                name,
                mass,
                gm: match gm {
                    Some(v) => v,
                    None => mass * self.settings.cos_g,
                },
                index,
            },
        );
        self.data.push(SimObject::new_new(index, start));
        index
    }
    fn add_constraint(&mut self, new_constraint: SimConstraints) {
        self.rules.push(new_constraint)
    }
    /**************************************************************************
     *
     */
    fn snapshot(&self, at: Second) -> SystemSnapshot {
        let mut snap: Vec<Snapshot> = Vec::from([]);
        for r in self.data.iter() {
            snap.push(r.snapshot(at, &self.bodies));
        }
        SystemSnapshot { system: snap }
    }
    pub fn print_overview(&self) {
        for o in self.data.iter() {
            if !o.is_empty() {
                println!("{}", o.report(&self.bodies));
            }
        }
    }

    /**************************************************************************
     *
     */
    pub fn advance_steps(&mut self, count: i32) -> Second {
        for _ in 0..count {
            self.step();
        }
        self.last_complete_timestamp()
    }

    /**************************************************************************
     *
     */
    pub fn advance_to_time(&mut self, to: Second) {
        while self.last_complete_timestamp() < to {
            self.step();
        }
    }

    /**************************************************************************
     *
     */
    pub fn advance_by_time(&mut self, by: Second) {
        self.advance_to_time(self.last_complete_timestamp() + by);
    }
    //Runs the simulation forward until all constraints are solved
    pub fn advance_until_conclusive(&mut self, force: Second) -> bool {
        assert!(force>PhysQuant::EPSILON*S);
        self.settings.force_step = Some(force);
        let mut failed = false;
        match self.rules.is_empty() {
            true => panic!("Can't conclusion until fail without any constraints"),
            false => {
                let mut inconclusive = true;
                while inconclusive && !failed {
                    self.advance_steps(1);
                    inconclusive = false;
                    for rule in &self.rules {
                        let r = rule.eval(self);
                        use ConstraintResult::*;
                        match r {
                            False(_) => failed = true,
                            Inconclusive(_) => inconclusive = true,
                            _ => (),
                        }
                    }
                }
            }
        }
        !failed
    }
    fn last_complete_timestamp(&self) -> Second {
        match self.data.is_empty() {
            true => 0.0 * S,
            false => {
                let mut time = self.data[0].last_complete_timestamp().unwrap().value_unsafe;
                for d in &self.data {
                    let d_time = d.last_complete_timestamp().unwrap().value_unsafe;
                    time = time.min(d_time);
                }
                time * S
            }
        }
    }
    pub fn report(&self) -> String {
        match self.data.is_empty() {
            true => String::from("no objects in system"),
            false => {
                let mut report = format!(
                    "System state report at ts={}",
                    self.last_complete_timestamp()
                );

                for d in &self.data {
                    report = format!("{}\n{}", report, d.report(&self.bodies));
                }
                for r in &self.rules {
                    report = format!("{}\n{}", report, r.eval(self).report());
                }

                report
            }
        }
    }
    pub fn rich_report(&self) -> RichStateReport {
        match self.data.is_empty() {
            true => RichStateReport {
                object_count: 0,
                final_time: 0.0 * S,
                report_lines: HashMap::<u16, RichStateReportItem>::new(),
            },

            false => {
                let mut report_lines = HashMap::<u16, RichStateReportItem>::new();

                for d in &self.data {
                    let report = d.rich_report(&self.bodies);
                    report_lines.insert(report.index, report);
                }
                RichStateReport {
                    object_count: self.bodies.len(),
                    final_time: self.last_complete_timestamp(),
                    report_lines,
                }
            }
        }
    }
}
enum ConstraintResult {
    True(String),
    False(String),
    Inconclusive(String),
}
impl ConstraintResult {
    fn report(&self) -> String {
        match self {
            Self::True(t) | Self::False(t) | Self::Inconclusive(t) => t.clone(),
        }
    }
}
/*
pub struct ConstraintResult {
    truth: bool,
    text: String,
    conclusive: bool,
}*/
pub enum SimConstraints {
    Distance {
        min: Meter,
        max: Meter,
        a: u16,
        b: u16,
        at: Second,
    },
    RefState {
        state: InstantState,
        primary: u16,
        relative_to: Option<u16>,
        allowed_error: Unitless,
    },
}
impl SimConstraints {
    fn eval(&self, sim: &Simulation) -> ConstraintResult {
        match self {
            Self::Distance { min, max, a, b, at } => {
                let at = at;
                let a_obj = sim.bodies.get(a).unwrap();
                let b_obj = sim.bodies.get(b).unwrap();
                match sim.last_complete_timestamp() < *at {
                    true => ConstraintResult::Inconclusive(format!(
                        "{} - {} distance check at {} can't be checked yet",
                        a_obj.name, b_obj.name, at
                    )),
                    false => {
                        let input = sim.snapshot(*at);
                        let a: Snapshot = input.by_id(*a).unwrap().clone();

                        let b: Snapshot = input.by_id(*b).unwrap().clone();
                        let a_pos = a.state.pos;
                        let b_pos = b.state.pos;
                        let d = (a_pos - b_pos).mag();
                        let text = format!(
                            " {} - {} distance: {:.2e} <= {:.2e} <= {:.2e}",
                            a.body, b.body, min, d, max
                        );
                        match d >= *min && d <= *max {
                            true => ConstraintResult::True(format!("{} PASS", text)),
                            false => ConstraintResult::False(format!("{} FAIL", text)),
                        }
                    }
                }
            }
            Self::RefState {
                state,
                primary,
                relative_to,
                allowed_error,
            } => {
                let a_obj = sim.bodies.get(primary).unwrap();
                match sim.last_complete_timestamp() < state.time {
                    true => ConstraintResult::Inconclusive(format!(
                        "state for {} at {} can't be checked yet",
                        a_obj.name, state.time
                    )),
                    false => {
                        let input = sim.snapshot(state.time);
                        let a = input.by_id(*primary).unwrap().state;
                        let b = match relative_to {
                            None => InstantState::default(),
                            Some(id) => input.by_id(*id).unwrap().state,
                        };
                        let real_p = a.pos - b.pos;
                        let real_v = a.vel - b.vel;
                        let p_diff: PosVec = state.pos - real_p;
                        let v_diff: VelVec = state.vel - real_v;
                        let p_error = p_diff.mag() / real_p.mag();
                        let v_error = v_diff.mag() / real_v.mag();
                        let truth =
                            p_error.value() < allowed_error && v_error.value() < allowed_error;
                        let text = format!(
                            "check passed?:{} allowed error {}, p_error {}, v_error  {}",
                            truth, allowed_error, p_error, v_error
                        );
                        match p_error.value() < allowed_error && v_error.value() < allowed_error {
                            true => ConstraintResult::True(text),
                            false => ConstraintResult::False(text),
                        }
                    }
                }
            }
        }
    }
}
