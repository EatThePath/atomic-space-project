
use crate::basic_state_types::*;
//Interface provides a shared state communication area between threads
#[derive(Default)]
pub struct Interface {
    pub report: String,
    pub ui_end: bool,
    pub sim_end: bool,
    pub report2: Option<RichStateReport>

}