

use crate::prelude::*;
use std::collections::HashMap;
use std::fmt::{self};
//Basic state vectors as a time
#[derive(Clone, Copy, Debug)]
pub struct InstantState {
    pub pos: PosVec,
    pub vel: VelVec,
    pub acl: AclVec,
    pub time: Second,
}

impl InstantState {
    pub fn extrapolate_to_timestamp(self: &InstantState, at: Second) -> Result<InstantState, bool> {
        let dt = at - self.time;
        let acl = self.acl;
        let vel = self.vel + (acl * dt);
        let pos = self.pos + vel * dt;
        Ok(InstantState {
            pos,
            vel,
            acl,
            time: at,
        })
    }
}
impl fmt::Display for InstantState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "p:({:.2e},{:.2e},{:.2e}) v:({:.2e},{:.2e},{:.2e}) a:({:.2e},{:.2e},{:.2e}) at t:{:.2e} ",
            self.pos.x,self.pos.y,self.pos.z,
            self.vel.x,self.vel.y,self.vel.z,
            self.acl.x,self.acl.y,self.acl.z, self.time)
    }
}
impl Default for InstantState {
    fn default() -> Self {
        InstantState {
            pos: PosVec {
                x: 0.0 * M,
                y: 0.0 * M,
                z: 0.0 * M,
            },
            vel: VelVec {
                x: 0.0 * MPS,
                y: 0.0 * MPS,
                z: 0.0 * MPS,
            },
            acl: AclVec {
                x: 0.0 * MPS2,
                y: 0.0 * MPS2,
                z: 0.0 * MPS2,
            },
            time: 0.0 * S,
        }
    }
}

pub type ObjectID = u16;

#[derive(Default, Clone)]
pub struct Snapshot {
    pub body: ObjectID,
    pub state: InstantState,
}

#[derive(Default, Clone)]
pub struct SystemSnapshot {
    pub system: Vec<Snapshot>,
}
impl SystemSnapshot {
    pub fn by_id(&self, id: ObjectID) -> Option<&Snapshot> {
        let mut o: Option<&Snapshot> = None;
        for i in self.system.iter() {
            if i.body == id {
                o = Some(i);
            }
        }

        o
    }
}

/****
 * Used for loading/saving system representations.
 */
pub struct InitialVectorConditions {
    pub name: String,
    pub mass: KGram,
    pub pos: PosVec,
    pub vel: VelVec,
    pub acl: AclVec,
}
impl ToString for InitialVectorConditions {
    fn to_string(&self) -> String {
        format!(
            "{} {} {} {} {}",
            self.name, self.mass, self.pos, self.vel, self.acl
        )
    }
}
impl Default for InitialVectorConditions {
    fn default() -> Self {
        InitialVectorConditions {
            pos: PosVec {
                x: 0.0 * M,
                y: 0.0 * M,
                z: 0.0 * M,
            },
            vel: VelVec {
                x: 0.0 * MPS,
                y: 0.0 * MPS,
                z: 0.0 * MPS,
            },
            acl: AclVec {
                x: 0.0 * MPS2,
                y: 0.0 * MPS2,
                z: 0.0 * MPS2,
            },
            name: String::new(),
            mass: 0.0 * KG,
        }
    }
}

#[derive(Default)]
pub struct BodyDef {
    pub name: String,
    pub mass: KGram,
    pub gm: GM,
    pub index: u16,
}
pub type BodyLookup = HashMap<u16, BodyDef>;

#[derive(Default, Clone)]
pub struct RichStateReportItem {
    pub name: String,
    pub index: u16,
    pub count: usize,
    pub mass: KGram,
    pub first_time: Second,
    pub last_time: Second,
}
#[derive(Default, Clone)]
pub struct RichStateReport {
    pub object_count: usize,
    pub final_time: Second,
    pub report_lines: HashMap<u16, RichStateReportItem>,
}
