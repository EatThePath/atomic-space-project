#[macro_use]
mod async_interface;
mod sim;
mod basic_state_types;
mod as_util;

pub mod prelude;
use crate::prelude::*;

use std::time::{Duration, Instant};
use std::sync::{Arc, Mutex};
use std::thread;
use async_interface::*;
use sim::*;
use clap::Parser;

/// Search for a pattern in a file and display the lines that contain it.
#[derive(Parser)]
#[clap()]
struct Args {
    #[clap(short, long, value_parser)]
    system: Option<String>,
}

fn main() {
    let interface = Arc::new(Mutex::new(Interface {
        ui_end: false,
        sim_end: false,
        ..Default::default()
    }));
    let shi = Arc::clone(&interface);
    let sh = thread::spawn(move || {
        sim(shi);
    });
    let uii = Arc::clone(&interface);
    let uih = thread::spawn(move || {
        ui(uii);
    });
    let _ = sh.join();
    let _ = uih.join();
}

fn sim(interface: Arc<Mutex<Interface>>) {
    let args = Args::parse();
    let mut u: Simulation;

    match args.system {
        Some(name) => u = Simulation::load_system(&name),
        None => u = Simulation::load_system(&"A".to_string()),
    }

    let run_start = Instant::now();
    let simsteps = 1000;
    let run_target = 365.0 * DAY * 20.0 * 10.0;
    let mut terminate = false;
    let mut report_string;
    let mut step_end = 0.0 * S;
    while !terminate && step_end < run_target {
        step_end = u.advance_steps(simsteps);

        let run_end = Instant::now();
        let realtime = (run_end - run_start).as_secs_f64();

        report_string = format!(
            "{}\nOverall sim {:2e} sim seconds\t in {:.6} real seconds,\t a factor of {:.2e}",
            u.report(),
            step_end,
            realtime,
            step_end / realtime
        );
        let mut i = interface.lock().unwrap();
        terminate = i.ui_end;
        i.report = report_string.clone();
    }
    let mut i = interface.lock().unwrap();
    i.sim_end = true;
}

fn ui(interface: Arc<Mutex<Interface>>) {
    let mut done = false;
    let mut display = String::new();
    while !done {
        thread::sleep(Duration::from_millis(750));
        println!("\n\n\n\n{display}");
        let i = interface.lock().unwrap();
        done = i.sim_end;
        display = i.report.clone();
    }
}
