

use crate::basic_state_types::{BodyLookup};
use crate::sim::*;
use std::vec;
pub mod timeline;


#[derive(Default)]
pub struct SimObject {
    body_key: u16,
    history: RawTimeline,
    controller: RollingGravMotion,
    next_state: Option<InstantState>,
}
impl SimObject {

    pub fn new_new(body_key: u16,start_state:InstantState ) -> SimObject {
        SimObject {
            history: RawTimeline::new(vec![start_state]),
            body_key,
            next_state: None,
            controller: RollingGravMotion{id:body_key},
            ..Default::default()
        }
    }
    pub fn step(self: &mut SimObject, to: Second, system: &SystemSnapshot, lookup:&BodyLookup, settings:&SimSettings) {
        let last_state = *self.history.last().unwrap();
        if last_state.time <= to {
            match self.controller.step(last_state, system, lookup, settings) {
                Ok(r) => {
                    self.next_state = Some(r);
                }
                Err(_) => {
                    self.next_state = None;
                }
            };
        } else {
            self.next_state = None
        }
    }
    pub fn post(self: &mut SimObject) -> Option<Second> {
        match self.next_state {
            Some(r) => {
                //println!("{}",r);
                self.history.extend(r);
                Some(r.time)
            }
            None => (None),
        }
    }
    pub fn snapshot(self: &SimObject, at: Second,dict:&BodyLookup) -> Snapshot {
        let bp = (dict.get(&self.body_key)).unwrap();
        //println!(" doing state search for time {} and obj {}",at,bp.name );
        Snapshot {
            body: bp.index,
            state: self.history.state_at_time(at).unwrap(),
        }
    }
    pub fn last_state(&self) -> Option<&InstantState> {
        self.history.last()
    }
    pub fn is_empty(&self) -> bool {
        self.history.is_empty()
    }
    pub fn report(&self, dict:&BodyLookup) -> String {
        format!(
            "({}){} has {}",
            self.body_key,
            (dict.get(&self.body_key)).unwrap().name,
            self.history.report()
        )
    }
    
    pub fn rich_report(&self, dict:&BodyLookup) -> RichStateReportItem {
        let mut r = self.history.rich_report();
        let b = (dict.get(&self.body_key)).unwrap();
        r.name = b.name.clone();
        r.index = self.body_key;
        r.mass = b.mass;
        r
    }

    pub fn last_complete_timestamp(&self) -> Option<Second> {
        match self.is_empty() {
            true => None,
            false => Some(self.history.last().unwrap().time),
        }
    }
}
