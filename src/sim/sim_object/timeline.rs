use crate::basic_state_types::*;
use crate::prelude::*;
use crate::pretty_time;
#[derive(Default)]
pub struct RawTimeline {
    states: Vec<InstantState>,
}
enum BinarySearchReturn {
    SearchError,
    After,
    Exact { result: InstantState },
    Before { result: InstantState },
}
/**************************************************************************
 *
 */
impl RawTimeline {
    fn state_search_slice(
        state: &Vec<InstantState>,
        min: usize,
        max: usize,
        target: Second,
    ) -> BinarySearchReturn {
        let range = max - min;
        //println!("{} {} {}", min, max, range);
        use BinarySearchReturn::*;
        match range {
            //panic!("state search was called on a zero length slice"),
            l if l == 0 => {
                //println!("r1");
                Self::state_search_evaluate_single(&state[min], target)
            }
            // Self::state_search_evaluate_single(&state[min], target),
            l if l == 1 => {
                //println!("r2");
                let r = Self::state_search_evaluate_single(&state[min], target);
                match r {
                    SearchError => SearchError,
                    Exact { result: _ } | Before { result: _ } => r,
                    After => Self::state_search_evaluate_single(&state[max], target),
                }
            }
            l if l > 1 => {
                //println!(">2");
                let split = min + ((range * 99) / 100); //biasing forward heavily because right now we almost always want to find near the end
                                                        //println!("       {}", split);
                let r = Self::state_search_evaluate_single(&state[split], target);
                match r {
                    SearchError => SearchError,
                    Exact { result: _ } => r,
                    Before { result: _ } => {
                        //println!("before, next {} {}", split.min(max), max);
                        Self::state_search_slice(state, split.min(max), max, target)
                    }
                    After => {
                        //println!("after, next {} {}", min, (split).max(min));
                        Self::state_search_slice(state, min, (split).max(min), target)
                    }
                }
            }
            _ => {
                println!("err");
                SearchError
            }
        }
    }
    fn state_search_evaluate_single(state: &InstantState, target: Second) -> BinarySearchReturn {
        // println!(
        //     "   bs, target {:.2e}, considering {:.2e}",
        //     target, state.time
        // );
        match state.time {
            l if l == target => BinarySearchReturn::Exact { result: *state },
            l if l > target => BinarySearchReturn::After,
            l if l < target => BinarySearchReturn::Before { result: *state },
            _ => BinarySearchReturn::SearchError,
        }
    }

    /**************************************************************************
     *
     */
    pub fn state_at_time(self: &RawTimeline, at: Second) -> Result<InstantState, bool> {
        //println!("\n**************\nsearching for state {}", at);
        use BinarySearchReturn::*;
        let r = Self::state_search_slice(&self.states, 0, self.states.len() - 1, at);
        match r {
            SearchError => panic!(
                "Unspecified search error for timeone of length {}",
                self.states.len()
            ),
            After => panic!(
                "Tried to find a state at {}, first state is at {}, last at {}",
                at,
                self.states[0].time,
                self.states.last().unwrap().time
            ),
            Exact { result: exact } => Ok(exact),
            Before { result: before } => Ok(before.extrapolate_to_timestamp(at)?),
        }
    }

    /**************************************************************************
     *
     */
    pub fn new(states: Vec<InstantState>) -> RawTimeline {
        RawTimeline { states }
    }

    /**************************************************************************
     *
     */
    pub fn last(self: &RawTimeline) -> Option<&InstantState> {
        self.states.last()
    }

    /**************************************************************************
     *
     */
    pub fn is_empty(self: &RawTimeline) -> bool {
        self.states.is_empty()
    }

    /**************************************************************************
     *
     */
    pub fn report(self: &RawTimeline) -> String {
        format!(
            "{} states\t final at {}",
            self.states.len(),
            pretty_time(self.states.last().unwrap().time)
        )
    }

    pub fn rich_report(self: &RawTimeline) -> RichStateReportItem {
        RichStateReportItem {
            count: self.states.len(),
            first_time: self.states[0].time,
            last_time: self.last().unwrap().time,
            ..Default::default()
        }
    }

    /**************************************************************************
     *
     */
    pub fn extend(self: &mut RawTimeline, next: InstantState) {
        self.states.push(next);
    }
}
