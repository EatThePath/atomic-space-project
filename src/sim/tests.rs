use super::*;
use crate::as_util::kepler::*;

//This does a crude search to find a timeset where a given keplerian orbit is in the right shape after a certain number of orbits.
//Doing this as a test is somewhat hacky and bad. I intend on building a new binary target for it.
fn run_kepler_stability_test(
    elements: KeplerElements,
    center_mass: KGram,
    orbits: PhysQuant,
    error: PhysQuant,
) {
    single_kepler_stability_test(elements,center_mass,orbits,error)
}

fn single_kepler_stability_test(
    elements: KeplerElements,
    center_mass: KGram,
    orbits: PhysQuant,
    error: PhysQuant,
) {
    let star_start = InstantState {
        pos: PosVec::new(0.0 * M, 0.0 * M, 0.0 * M),
        vel: VelVec::new(0.0 * MPS, 0.0 * MPS, 0.0 * MPS),
        ..Default::default()
    };
    let earth_start = InstantState {
        pos: elements.relative_position(0.0),
        vel: elements.relative_velocity(center_mass, 0.0),
        ..Default::default()
    };
    let new_ta = orbits*TAU;
    let earth_final = InstantState{
        pos:elements.relative_position(new_ta),
        vel:elements.relative_velocity(center_mass, new_ta),
        time: elements.period(center_mass).unwrap() * orbits,
        ..Default::default()
    };
    stab_test_loop(star_start,center_mass,earth_start,elements,earth_final,error);
}

fn stab_test_loop(
    primary: InstantState,
    primary_mass: KGram,
    secondary: InstantState,
    ref_orbit: KeplerElements,
    target: InstantState,
    error: PhysQuant,
) {
    let orbital_period = ref_orbit.period(primary_mass).unwrap();
    let mut step = 60.0 * 60.0 * S;
    let mut seeking = true;
    let mut dir = 0;
    let mut good_report = String::new();
    let mut good_step = 0.0 * S;
    let mut last_step = step;
    let mut refine = 2.0; 
    while seeking {
        //set up the simulation!
        println!("Trying step size:{}",step);
        let (pass,report) =  stab_test_loop_inner(
            primary,
            primary_mass,
            secondary,
            step,
            target,
            error
        );
        if pass {
            good_report = report.clone();
            good_step = step;
        }
        println!("{}",report.clone());
        println!(
            "loop:{}\t passing:\t{} \tdir:{} \tstep:{} \tadjust:{}",
            seeking, pass, dir, step, refine
        );
        if seeking && dir == 0 {
            //first test, check if it passed or not;
            if pass {
                dir = 1;
                step *=  2.0;
                good_step = step;
            } else {
                dir = -1;
                step /= 2.0;
            };
        } else if dir > 0 {
            if pass {
                step *= refine;
            } else if refine - f64::max(refine * 0.1, 0.05) > 1.0 {
                step /= refine;
                refine -= f64::max(refine * 0.1, 0.05);
                step *=  refine;
            } else {
                seeking = false
            };
        } else if dir < 0 {
            if !pass && refine > 1.0  {
                last_step = step;
                step /=  refine;
            } else if refine - f64::max(refine * 0.1, 0.05) > 1.0 {
                step = last_step;
                refine -=  f64::max(refine * 0.1, 0.05);
                step /= refine;
            } else {
                seeking = false;
            };
            println!(
                "loop:{}\t passing:\t{} \tdir:{} \tstep:{} \tadjust:{}",
                seeking, pass, dir, step, refine
            );
        }
    }
    println!("last passing step size\n{}", good_report);
    println!(
        "{:.2}, {:.0} steps per orbit",
        good_step,
        orbital_period / good_step
    );
}

fn stab_test_loop_inner(
    primary: InstantState,
    primary_mass: KGram,
    secondary: InstantState,
    step: Second,
    target: InstantState,
    error: PhysQuant,
) -> (bool,String) {
    let mut sim = Simulation {
        ..Default::default()
    };
    sim.add_body(primary, String::from("center"), primary_mass, None);

    sim.add_body(secondary, String::from("follow"), 0.0 * KG, None);
    sim.add_constraint(SimConstraints::RefState{
        state:target,
        primary:1,
        relative_to:Some(0),
        allowed_error:Unitless::new(error),
    });
    //run the simulation!
    (sim.advance_until_conclusive(step),sim.report())
} 

#[cfg(test)]
#[test]
fn test() {
    let radius = AU;
    let center_mass = M_S;
    let k = kepler::KeplerElements {
        a: radius,
        e: Unitless::new(0.00),
        i: Unitless::new(0.0),
        node: 0.0,
        arg_of_peri: 0.0,
        true_anomaly: Some(0.0),
    };
    run_kepler_stability_test(k,center_mass,1.687,0.001);
}
